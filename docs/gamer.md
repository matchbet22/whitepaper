# Gamer

## Jogador e investidor

O jogador ideal que buscamos para utilizar a MatchBet é aquela pessoa, acima de 18 anos, que tenha interesse em tecnologia blockchain e NFTs; um conhecimento básico no mercado de criptoativos, visto que há a necessidade de se adquirir um criptoativo para poder ingressar na plataforma e todas as compras e recebimentos são realizados em Betcoin; que está inserido na comunidade de jogos eletrônicos, ou seja, um jogador que possui diversos amigos e/ou conhecidos que também jogam online e que tenha interesse em gerar renda paralelamente a sua diversão.
 
Além do jogador ideal, temos o investidor: aquela pessoa, acima de 18 anos, que veja o potencial da plataforma e que perceba a oportunidade de se adquirir um ativo que tende a valorizar muito em mercado em franca ascensão; haja vista se fundar em um projeto sólido, onde a maior parte de seu desenvolvimento já foi realizado por investimento privado e foi desenvolvido por uma equipe que é liderada por um programador experiente, que possui um currículo impecável, tendo trabalhado nas maiores agências de desenvolvimento de sistemas críticos do mundo, como a NASA e Agência Espacial Europeia.
