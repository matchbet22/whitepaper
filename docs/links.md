## Informações Relevantes
- Website oficial: [http://www.matchbet.io](http://www.matchbet.io)
- Data do ICO (T.B.D.).
- A Betcoin é um utility token e será listada em diversas corretoras mundiais, como a Pancakeswap.

## Links de interesse
- [https://oglobo.globo.com/rio/bairros/depois-da-sonda-perserverance-brasileiro-trabalha-na-missao-da-nasa-que-levara-primeira-mulher-lua-24922720](https://oglobo.globo.com/rio/bairros/depois-da-sonda-perserverance-brasileiro-trabalha-na-missao-da-nasa-que-levara-primeira-mulher-lua-24922720)
- [https://www.metropoles.com/brasil/vida-em-outros-planetas-conheca-o-brasileiro-que-trabalha-com-a-nasa](https://www.metropoles.com/brasil/vida-em-outros-planetas-conheca-o-brasileiro-que-trabalha-com-a-nasa)
- [https://www.terra.com.br/istoegente/43/reportagens/rep_detetive.htm](https://www.terra.com.br/istoegente/43/reportagens/rep_detetive.htm)
- [https://vejario.abril.com.br/coluna/beira-mar/wanderley-de-abreu-junior-ex-hacker-que-invadiu-a-nasa-inaugura-nova-sede-de-fabrica-de-drones-no-rio/](https://vejario.abril.com.br/coluna/beira-mar/wanderley-de-abreu-junior-ex-hacker-que-invadiu-a-nasa-inaugura-nova-sede-de-fabrica-de-drones-no-rio/)
- [https://www.bbc.com/portuguese/brasil-48978801](https://www.bbc.com/portuguese/brasil-48978801)
- [![Wanderley de Abreu Jr "Storm" no lançamento do Hacking.Rio](http://img.youtube.com/vi/ikPnSn8gV_s/0.jpg)](https://www.youtube.com/watch?v=ikPnSn8gV_s "Wanderley de Abreu Jr \"Storm\" no lançamento do Hacking.Rio")
- [https://gauchazh.clicrbs.com.br/tecnologia/noticia/2021/02/brasileiro-envolvido-em-missao-da-nasa-a-marte-revela-detalhes-e-lamenta-atraso-em-programa-espacial-no-pais-cklmphfxk005h01668uub5xb4.html](https://gauchazh.clicrbs.com.br/tecnologia/noticia/2021/02/brasileiro-envolvido-em-missao-da-nasa-a-marte-revela-detalhes-e-lamenta-atraso-em-programa-espacial-no-pais-cklmphfxk005h01668uub5xb4.html)
- [https://piaui.folha.uol.com.br/materia/o-ceu-de-wanderley/](https://piaui.folha.uol.com.br/materia/o-ceu-de-wanderley/)
- [Medalha Tiradentes](http://alerjln1.alerj.rj.gov.br/scpro1923.nsf/10d6d451b00fd42b832566ec0018d836/272ef42d349f884203258696006c74f1?OpenDocument)
- [Medalha Pedro Ernesto](http://mail.camara.rj.gov.br/APL/Legislativos/contlei.nsf/874a3fe356a1d6970325774c006180d5/4a71f3f276efe2340325876500607f3b?OpenDocument)
