# Mercado de Jogos

## O Mercado de jogos online
A indústria de games, que já era maior do que a indústria da música e do cinema, juntas antes da pandemia foi ainda mais impulsionada neste período. Esse crescimento se deu pela combinação de isolamento social somado à busca por novas formas de entretenimento e interação social. Segundo a Accenture consultoria, o mercado de jogos eletrônicos movimenta U$ 300 bi ao ano e, em 2021, no Brasil, por exemplo, cresceu mais de 140%.
 
Para se ter uma ideia, o jogo League of Legends alcançou o incrível número de mais de 180 milhões de usuários ativos mensais (Fonte: Riot Games). O Dota 2, que vem em segundo lugar, tem mais de 600 mil jogadores ativos diariamente, com um total de mais de 48 milhões de usuários registrados (Fonte: SteamDB).
 
Atualmente, os canais de esporte transmitem, com grande audiência, competições de jogos online em suas grades e foram criadas plataformas de streaming dedicadas a games, caso da Twitch.
 
O mercado de jogos é popular em todos os continentes e estima-se que há, pelo menos, 3,4 bilhões de jogadores no mundo e que eles estão gastando, cada vez mais, tempo e dinheiro jogando. Além disso, jogar vem se tornando não apenas uma atividade social e comunitária, mas, sobretudo, uma atividade profissional.
 
Espera-se que, nos próximos anos, mais 400 milhões de novos jogadores ingressem na indústria de jogos online e, aqueles que já se encontram estão jogando em consoles, dispositivos móveis e PCs. Mas, aonde eles estão jogando é tão significativo quanto com quem estão jogando, pois 84% dos jogadores dizem que os jogos os ajudam a se conectar e conhecer novas pessoas. Durante a pandemia, três quartos dos jogadores disseram que uma maior parte de suas interações sociais acontecem em uma plataforma de jogo. (Fonte: Accenture)
 
Com o crescimento explosivo dos jogos online e o aumento do desejo do jogador por interações sociais, o jogo não é mais alinhado exclusivamente com o produto, mas, predominantemente, na experiência do usuário. Dessa forma, a indústria de jogos está cada vez mais se tornando uma indústria de serviços, em que os usuários podem dispor de todo um sistema que os ajude a ter uma experiência mais ampla, não se limitando apenas a prática dos jogos.

## Nossa visão sobre o Mercado

Mesmo sendo uma indústria que existe há algum tempo, há cada vez mais possibilidades de se empreender e utilizar o próprio mercado de games a favor de outros negócios, desenvolvendo ferramentas e plataformas para diversas atividades. A MatchBet nasceu com essa visão de oportunidade: não há uma grande exploração do mercado de disputas entre usuários na indústria de jogos, utilizando um criptoativo próprio, para intermediar partidas entre os próprios usuários, possibilitando momentos de diversão e ganhos, com ferramentas que tragam uma excelente experiência para quem a utiliza; bem como uma possibilidade de aumento em sua renda, seja por intermédio das disputas, das suas indicações ou pela valorização da sua moeda.

Assim, a MatchBet não mudou a sua visão ao longo do projeto: nós desejamos que todos os gamers possam aproveitar uma plataforma que se preocupa em trazer diversão com geração de renda; criar diversas oportunidades para aqueles que estão inseridos na comunidade gamer encontrem a melhor maneira de interação com os outros usuários, seja criando torneios, se desafiando, trazendo usuários para a plataforma, etc., mas que cada um encontre a sua melhor vocação para se divertir e gerar renda. E, fazendo isso, que todos contribuam para um ambiente saudável, divertido e harmônico, colaborando para a valorização da moeda e para o crescimento de ótimos serviços no universo gamer, conectando cada vez mais os jogadores.
