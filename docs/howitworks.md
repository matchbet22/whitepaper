# Como a plataforma funciona
A MatchBet, como vimos, é um site responsivo que tem como premissa fundamental intermediar disputas, entre os usuários ou times de usuários, em jogos online de habilidade, fora do ambiente blockchain, utilizando para tanto o seu criptoativo próprio, a Betcoin.

O site é responsivo para adequar sua visualização ao tamanho de tela em que está sendo exibido, tendo em vista a ampla diversidade de equipamentos utilizados para acesso à internet (como: computadores, tablets e celulares).
No entanto, apesar de ser fundamentado nas disputas entre usuários para que o vencedor adquira as Betcoins, ele possui diversas outras funcionalidades e mecânicas mostradas a seguir:

## Dinâmica da MatchBet
Para que um usuário ganhe bônus de indicações e outros bônus, realize qualquer disputa, crie torneios, ele deve, primeiramente, adquirir a NFT (Token não fungível) correspondente à funcionalidade que ele deseja habilitar dentro da plataforma. Ou seja, caso o usuário não possua qualquer NFT ou se for o caso de sua NFT ter expirado, ele apenas poderá ser convidado para as disputas, mas não será possível iniciá-las e não será possível receber seus bônus.
 
As NFTs são um tipo de certificado de autenticidade para bens digitais estabelecidos por meio da blockchain. A NFT é um token criptográfico que representa algo único, indivisível e exclusivo e sua autenticidade pode ser confirmada e jamais alterada.

Portanto, as NFTs que o usuário possui em seu inventário automaticamente destravam e permitem que o usuário acesse aquela determinada funcionalidade correspondente à descrição de cada NFT. As NFTs poderão ser vitalícias, somente edições especiais, ou terão um prazo de validade determinado, normalmente de um ano após a sua aquisição.
 
As NFTs podem ser adquiridas na Loja da MatchBet ou no mercado, através de plataformas que negociam NFTs. Elas podem ser negociadas livremente pelos usuários nas plataformas blockchain que fazem transações de NFTs entre pessoas.  Na MatchBet as NFTs podem se tornar itens colecionáveis,
*Caso o usuário adquira ou pretenda adquirir a sua NFT no mercado, a MatchBet disponibilizará, no marketplace, um histórico com todas as NFTs já desenvolvidas para que o usuário possa visualizar as características da NFT que ele adquiriu ou pretende adquirir.

Sendo assim, vamos conhecer as diferentes NFTs e o que elas permitem aos usuários realizarem dentro da MatchBet:

### NFT de Disputas entre Amigos e Indicações (MatchBet)
Através desta NFT, o usuário poderá colocar Betcoins em disputas criadas por ele contra usuários que estão na sua Lista de Amigos. E, além disso, ele poderá começar a ganhar bônus com as suas indicações de usuários para a MatchBet. O percentual e a forma de Bônus será definido em cada NFT disponibilizada para venda pela MatchBet.

### NFT de Aposta Randômica (SuperBet)
Com a SuperBet é possível que um usuário entre em uma disputa criada por ele ou por um usuário que não esteja na sua lista de amigos. Portanto, trata-se de um desafio randômico, em que ele procura um usuário com as características de disputa semelhantes a dele e a MatchBet busca o seu adversário.

### NFT de Criação de Torneios (ChampBet)
Muito mais divertido do que participar de uma disputa simples é jogar um torneio, com chances de ganhar todas as Betcoins em disputa. Porém, para criar um torneio, o usuário deverá possuir essa NFT. Com o término do torneio, o usuário recebe um percentual sobre as Betcoins que foram colocadas em disputa. A forma e o percentual a ser recebido será detalhado em cada NFT disponibilizada no marketplace da MatchBet.

### NFT de Cashback nas Apostas (CashBet)
A MatchBet retém um rake, ou seja, um pequeno percentual em todas as disputas realizadas na sua plataforma. E, com essa NFT, parte desse percentual retorna para o usuário que a possuir. Sendo assim, a MatchBet divide os seus ganhos com os seus usuários. O percentual e a maneira como será efetuado o cashback será informado na descrição da NFT na loja.

### NFT de CashBack nas Apostas realizadas pelos Indicados (Super CashBet)
Quando um usuário indica outro usuário para entrar na MatchBet e ele se cadastra, esse usuário fica listado na aba “Indicação de Usuários” daquele que o indicou. Quando um usuário possui a NFT de Super CashBet, ele ganha um percentual do rake da plataforma em todas as disputas realizadas por aqueles usuários de sua lista de indicados. Realmente é um Super CashBet!

O usuário terá todas as informações acerca dos percentuais e a forma de como serão os seus ganhos descritos nas características das NFTs que disponibilizarmos no marketplace.

### NFT de CashBack na compra de NFTs dos Indicados (SalesCash)
Toda compra de NFTs realizada na loja da MatchBet, por um dos usuários que estão na Lista de Indicados, gera um bônus que é creditado diretamente na conta do usuário que o indicou, claro, se esse usuário tiver em seu inventário a NFT SalesCash. Esse bônus e a forma como funciona a NFT será detalhada na loja.

### NFT de Cashback em TODAS as Apostas da Plataforma (MatchBet Partner)
A MatchBet Partner é a NFT que possibilita que o usuário que a possui tenha uma participação sobre parte dos resultados da MatchBet. Assim, no início de cada mês, quem tiver a NFT MatchBet Partner receberá parte de todas as disputas realizadas no mês anterior. Incrível! Quando essa NFT for disponibilizada no marketplace, sua descrição conterá todas as informações relevantes sobre o seu funcionamento.

### NFT de maior premiação por Rankeamento (RankCash)
Todo duelo disputado de forma randômica gera pontos de rankeamento para o usuário. Esses pontos definem a posição de cada usuário no ranking e como ele se encontra perante os demais usuários. Com a RankCash será possível ganhar um percentual extra nas disputas em duelos randômicos. Assim, mesmo que o usuário perca a disputa, ele recuperará parte de suas moedas. Todas as características das NFTs de RankCash serão detalhadas no marketplace.

### NFT Projeto Social MatchBet (MatchHealth)
A MatchBet sabe que a obesidade tem crescido entre crianças e adolescentes nos últimos anos. Os números crescem em decorrência de hábitos alimentares inadequados associados a um estilo de vida sedentário, que muitas vezes é motivado em razão do uso indiscriminado de dispositivos eletrônicos. Pensando nisso, criamos a MatchHealth, uma NFT em que todo o valor arrecadado com a sua venda, em nossa loja, será integralmente convertido em ações de combate à obesidade infantil.
Além de ajudar essa importante causa social, o usuário que a adquirir receberá 250,000 [FairPlay Points](#fairplay_points).

Os usuários poderão acompanhar as iniciativas da MatchBet no combate à obesidade infantil através dos nossos informativos.

## Dinâmicas das Disputas
### Disputas entre Amigos
Para criar uma disputa para desafiar seus amigos com Betcoins, o usuário deverá selecionar o jogo que pretende disputar na sua tela inicial, se tiver a opção de jogar em times, selecionar o time, o valor que gostaria de colocar em disputa e selecionar o amigo de sua Lista de Amigos.

O convite então será enviado para o seu amigo que poderá aceitar ou recusar o desafio. Caso o amigo aceite a disputa, o chat será iniciado, o valor correspondente será retirado de ambas as contas dos usuários e ela terá início.

Nesse momento, os usuários irão disputar a partida em um ambiente externo ao site e ao ambiente blockchain, no console e jogo que escolheram, adicionando os nicknames informados no seu perfil para aquele jogo online, sempre podendo se comunicar através do chat. É o procedimento comumente utilizado pelos gamers para disputar jogos online.

Os usuários então, após o fim da partida, preencherão os resultados. Caso os resultados sejam preenchidos da maneira correta, a validação é desnecessária. Caso haja uma controvérsia em relação ao resultado, as etapas de validação serão acionadas, com consequências para o jogador que informou o resultado que não condiz com a realidade do jogo.

Por conseguinte, o jogador vencedor levará o valor correspondente às Betcoins em disputa, que aparecerão em sua conta e a MatchBet reterá o seu percentual de _rake_*. A disputa será fixada no histórico do usuário e encerrada.

* O percentual de rake cobrado pela MatchBet é de 10% sobre o valor das Betcoins colocadas em disputa. Aqui estão incluídos todos os custos da plataforma, lucros, manutenção e retiradas de percentuais definidos nas NFTs para os usuários.

### Disputas Randômicas
Para criar uma disputa usando Betcoins com usuários da MatchBet que não estão na lista de amigos do usuário, ele deverá selecionar o jogo que pretende disputar na sua tela inicial, se tiver a opção de jogar em times, selecionar o time, o valor que gostaria de colocar em disputa e selecionar a opção de “buscar adversário”.

A plataforma irá buscar, dentre os usuários que desejam disputar uma partida de forma randômica, aqueles com as mesmas características de disputa. Assim, encontrando-se um usuário compatível, o convite será enviado e ele poderá aceitar ou recusar a partida. Caso o usuário aceite a disputa, o chat será iniciado, o valor correspondente será retirado de ambas as contas dos usuários e ela terá início.

Nesse momento, os usuários irão disputar a partida em um ambiente externo ao site e ao ambiente blockchain, no console e jogo que escolheram, adicionando os nicknames informados no seu perfil para aquele jogo online, sempre podendo se comunicar através do chat. É o procedimento comumente utilizado pelos gamers para disputar jogos online.

Os usuários então, após o fim da partida, preencherão os resultados. Caso os resultados sejam preenchidos da maneira correta, a validação é desnecessária. Caso haja uma controvérsia em relação ao resultado, as etapas de validação serão acionadas, com consequências para o jogador que informou o resultado que não condiz com a realidade do jogo.
Por conseguinte, o jogador vencedor levará o valor correspondente às Betcoins em disputa, que aparecerão em sua conta e a MatchBet reterá o seu percentual de rake. Além disso, ele receberá os respectivos pontos no ranking. A disputa será fixada no histórico do usuário e encerrada.

#### FairPlay Points
Os FairPlay Points são pontos que os usuários acumulam por completarem boas ações na MatchBet. E, no caso das disputas randômicas, os usuários os recebem através do encerramento de partidas bem-sucedidas, ou seja, que preencham o resultado da partida da forma mais honesta, informando corretamente quem ganhou e quem perdeu a disputa.

Quando ambos os usuários, tanto o vencedor, quanto o adversário que perdeu a partida, concluírem com êxito uma disputa randômica, ambos receberão 10,000 FairPlay Points.

Os FairPlay Points são essenciais para que um usuário consiga apostar mais Betcoins nas disputas randômicas; e, também, para que a MatchBet tente sempre unir, nessas disputas, usuários que possuam valores próximos de FairPlay Points acumulados.

Os FairPlay Points que um usuário possui poderão ser visualizados pelo seu oponente, ou seja, o usuário poderá decidir se aceita ou não a disputa, com base no número de FairPlay Points do seu oponente. Portanto, é aconselhável que os usuários acumulem muitos FairPlay Points.

E, de outra forma, caso um usuário preencha de forma errada o seu resultado e a MatchBet confirme que houve uma tentativa de enganar o sistema, esse usuário perderá TODOS os seus FairPlay Points, independentemente da quantidade que ele possuir. Por exemplo, se um usuário possui 1 milhão de FairPlay Points (100 partidas preenchidas com o resultado correto) e ele tenta enganar o sistema, uma única vez, e essa tentativa for confirmada pela MatchBet, lhe serão tirados todos os seus pontos. Desse modo, ele deverá iniciar sua caminhada novamente com zero FairPlay Points, sem prejuízo das outras punições cabíveis.

Os FairPlay Points são pessoais e intransferíveis.

### Criação e Participação em Torneios
O usuário para criar o seu próprio torneio deverá entrar na aba Torneios e selecionar o botão de criar torneios. Em seguida, ele deverá preencher as informações básicas do torneio, como data de início e hora, número de participantes, valor que cada usuário deverá contribuir para a premiação final do torneio, quantas fases do torneio serão disputadas por dia e número de jogadores. Depois, ele deverá selecionar os usuários ou times da sua Lista de Amigos que ele pretende convidar para o Torneio.

Os seus Amigos poderão aceitar ou recusar o seu convite. Caso algum Amigo recuse, outro Amigo poderá ser convidado em seu lugar. O Torneio apenas ficará disponível para aqueles usuários ou times que aceitaram o convite e todos poderão se comunicar através do chat da sala do torneio.

Caso o Amigo aceite, quando todos os integrantes correspondentes ao número de jogadores estabelecido pelo torneio tiverem sido preenchidos e o torneio iniciado pelo usuário que o criou, o valor em disputa será retirado da sua conta e somado aos valores dos outros integrantes.

Os usuários ou times que estão participando do torneio poderão visualizar todas as fases, bem como todas as chaves até a disputa final. Eles terão acesso a todas as informações do torneio que são facilmente visualizadas através da tela principal do torneio e são preenchidas automaticamente pela MatchBet.

As disputas ocorrem de forma semelhante às disputas com amigos, no entanto não há convite ou escolha do valor, já que o valor acertado fora anteriormente depositado e o convite para participar do torneio aceito.

Portanto, na data e hora que a partida deve ser iniciada entre os jogadores, cada participante receberá um aviso em sua tela e a disputa será iniciada automaticamente.

Nesse momento, os usuários irão disputar a partida em um ambiente externo ao site e ao ambiente blockchain, no console e jogo que escolheram, adicionando os nicknames informados no seu perfil para aquele jogo online, sempre podendo se comunicar através do chat. É o procedimento comumente utilizado pelos gamers para disputar jogos online.

Os usuários então, após o fim da partida, preencherão os resultados. Caso os resultados sejam preenchidos da maneira correta, a validação é desnecessária. Caso haja uma controvérsia em relação ao resultado, as etapas de validação serão acionadas, com consequências para o jogador que informou o resultado que não condiz com a realidade do jogo.

O jogador ou time que venceu a disputa passa de fase até a final do torneio, caso ganhe a final, será sagrado campeão e receberá os valores depositados no torneio, menos o valor sobre o percentual dos rakes das disputas enviado ao criador do torneio e o valor retido pela MatchBet em razão do seu percentual de rake das disputas.

Para participar de um torneio, basta aceitar o convite enviado pelo criador do torneio.

Eventualmente nossos parceiros ou a própria MatchBet poderá criar torneios abertos para os usuários da plataforma. Para participar desses torneios, basta o usuário clicar em Participar, quando for aberto o período de inscrições. Esses torneios aparecerão em destaque na aba Torneios e será enviado um alerta para os usuários.

## Integração com a MetaMask
Os jogadores deverão instalar a extensão em seu navegador para utilização da carteira MetaMask, que é a carteira indicada pela Binance para realizar transações em ambientes web.

O usuário, então, deverá criar uma conta na MetaMask para que, posteriormente, a sua carteira possa se conectar ao site MatchBet e possibilitar que ele adquira itens, carregue sua conta e receba suas moedas em sua carteira de usuário.

O usuário, após adquirir suas Betcoins e adicioná-las a sua carteira MetaMask, deverá carregar sua carteira do usuário no site. As moedas disponíveis na sua carteira de usuário serão aquelas utilizadas para transações no site, não serão utilizadas as moedas que estiverem na carteira MetaMask. Pois, as moedas carregadas ficarão em uma carteira da MatchBet, para facilitar o envio de moedas entre os usuários e entre a plataforma e os usuários.

## Cadastro e Login na plataforma
Os novos usuários poderão se cadastrar na MatchBet através de um formulário de cadastro ou por intermédio das suas redes sociais. No entanto, ao se conectar com suas redes sociais, os usuários serão convidados a preencher o formulário completo quando chegar o momento de transacionar fundos entre as suas carteiras e a MatchBet.

As informações concernentes aos consoles ou plataformas que o usuário utiliza para jogar seus jogos eletrônicos deverão ser preenchidas nessa fase, para que apareça em sua tela inicial todas as opções de jogos disponíveis no momento no ambiente que o jogador deseja disputar, videogame ou PC, bem como seus nicknames e Id´s, quando for pertinente. Em um outro momento de evolução da plataforma, haverá também a opção de dispositivos móveis.

Os usuários que preencherem o seu cadastro completo e concluírem a verificação de identidade da MatchBet receberão 100,000 [FairPlay Points](#fairplay_points)

Apenas usuários maiores de 18 anos poderão ter contas na MatchBet e só será permitida uma conta por usuário.

## Tela Inicial
A tela inicial contém todas as informações e principais funções disponíveis na plataforma para os nossos usuários. Ela será iniciada quando o usuário fizer o login na MatchBet. Nela estão:

- Nickname e caracterização do usuário na MatchBet
- A quantidade de moedas disponíveis na conta do usuário
- Quantidade de FairPlay Points
- Notificações
- Lista de Amigos
- Ranking
- Histórico de Disputas e Torneios
- Menu contendo: Perfil, Termos e Condições de Uso, Políticas de Privacidade, Configurações, SAC e Encerrar Sessão
- MatchBet Bank
- Os Consoles
- Os Jogos
- A Loja
- Torneios
- Times
- Inventário de NFTs
- Indicação de Usuários

## Times
Os usuários podem criar seus times para disputar jogos em que eles são necessários. Os times ficam na aba Times da página inicial. O usuário pode criar diversos times, personalizando cada um, inclusive adicionando imagens ao perfil do time. Porém, nesse momento, os times se comportam, para a MatchBet, como usuários simples, inclusive pontuando no ranking, nas disputas randômicas.

Em um próximo momento temos diversas ideias para ampliar as funcionalidades dos times e a integração de seus usuários. No entanto, atualmente, o usuário participará com o seu time, mas só o usuário que o criou pode ingressar com o time nas diversas formas de disputa.

Sendo assim, o time, como as disputas são realizadas em ambiente externo à MatchBet, possui uma utilidade para caracterizar os gamers que normalmente jogam com sua nomenclatura, mas, para a MatchBet, eles se comportam como se fossem usuários simples. Desse modo, gamers que não são usuários da MatchBet podem jogar no time criado pelo usuário. A MatchBet não possui esse controle externo de quem está jogando em cada time. No entanto, os bônus das vitórias e os ônus das derrotas, bem como todos os ganhos, irão apenas para o usuário que criou o Time.

## Histórico de Disputas e Torneios
Todas as partidas e torneios disputados por um usuário ficarão gravadas em seu histórico, contendo todas as informações relevantes, como data, resultado, valores disputados, adversários, jogo e horário da disputa.

## Lista de Amigos
O usuário poderá convidar outros membros da plataforma para fazer parte da sua Lista de Amigos. Os integrantes da Lista de Amigos de um usuário são aqueles que podem ser chamados para uma disputa entre amigos ou para os torneios.

O usuário deverá selecionar a opção de adicionar amigo, buscar seu amigo dentre aqueles usuários cadastrados na MatchBet e após selecionar o seu amigo na lista clicar em adicionar. O usuário selecionado receberá um convite para fazer parte da Lista de Amigos e poderá recusar ou aceitar participar.

Caso aceite participar, esse usuário fará parte da sua Lista de Amigos.

## Ranking
O ranking da MatchBet utiliza a fórmula de Elo Rating para calcular as pontuações dos usuários. As disputas que geram pontos no ranking são apenas as disputas randômicas, ou seja, aquelas em que o usuário não escolhe o seu oponente, estes são designados pela plataforma em uma busca de usuários que desejam colocar suas Betcoins em partidas com características semelhantes.

A posição de um usuário no ranking pode lhe gerar bônus e premiações ofertadas pela MatchBet em razão de seu desempenho, dependendo das campanhas realizadas na plataforma; elas serão previamente avisadas aos usuários para que eles tenham tempo para disputar as partidas que os levarão mais perto do topo do ranking.

## Notificações
Todas as notificações relevantes ao usuário serão enviadas com um alerta que fica na tela inicial. Todas as notificações ficam gravadas em ordem cronológica de recebimento para que o usuário as consulte com maior facilidade.

Os alertas, caso não sejam visualizados, permanecerão em destaque na área de notificações para que o usuário não se esqueça de verificar a informação que a MatchBet lhe enviou.

## Loja
A loja da MatchBet pode ser acessada pela página inicial do usuário. Na loja, o usuário encontrará à venda as nossas NFTs contendo todas as informações sobre seu uso, além de produtos oficiais que a MatchBet decidir colocar à venda, como peças de vestuário, canecas, bonés, acessórios gamers, etc.

Na loja o usuário também poderá encontrar produtos de nossos parceiros comerciais que eles poderão adquirir utilizando suas Betcoins.

## Inventário de NFTs
Todas as NFTs adquiridas pelo usuário deverão ser integradas ao inventário do usuário. Esse inventário será visível na página inicial e conterá todas as NFTs ativas que o usuário possui.

As NFTs adquiridas na loja da MatchBet serão incluídas automaticamente no seu inventário. No entanto, o usuário deverá requisitar a inclusão, em seu inventário das NFTs, dos ativos que ele, porventura, adquirir fora do ambiente da loja. Isso será feito de forma muito simples.

A MatchBet irá indicar marketplaces de venda e compra de NFTs para facilitar que os usuários encontrem outras pessoas que desejem vender ou comprar suas NFTs. Porém, o usuário tem a liberdade de comprar ou vender seus ativos nos marketplaces de sua preferência.

## Indicação de Usuários
Os usuários, como mencionado anteriormente, poderão indicar outras pessoas para se cadastrar na plataforma. Essa é uma funcionalidade muito importante, porque o usuário pode ganhar muitas Betcoins com essas indicações.

A aba Indicação de Usuários ficará na tela inicial e lá estarão listados todos os usuários que se cadastraram na MatchBet pela sua indicação.

Nesse mesmo ambiente se encontrará o link exclusivo do usuário, que possibilita que a MatchBet saiba quem foi o usuário que indicou o novo usuário cadastrado; e, por conseguinte, o incluirá, automaticamente, na lista de indicação de usuários que entraram na plataforma utilizando aquele link.

Desse modo, o usuário saberá quem utilizou o seu link de indicação para ingressar na plataforma e poderá receber todos os benefícios decorrentes dessas indicações.

Portanto, o usuário que quiser indicar pessoas para se cadastrar na MatchBet deverá apenas copiar esse link exclusivo que ele possui, na aba Indicação de Usuários, e enviar para as pessoas que ele acredita serem potenciais usuários da MatchBet. Essas pessoas então, ao se cadastrarem na MatchBet através desse link, farão parte da sua lista de indicados. Quanto mais indicações, mais o usuário poderá ganhar em Betcoins ao longo do tempo.

## Menu
No Menu, que estará visível na página inicial, estarão listadas funcionalidades que o usuário não utiliza com frequência em seu dia a dia, como: Informações do seu Perfil, Termos e Condições de Uso, Políticas de Privacidade, Configurações, FAQ, Suporte e Encerrar Sessão.

Informações de Perfil é uma compilação dos principais dados do usuário, como: nome, e-mail, imagem, número de moedas, número de documento, nicknames, data de nascimento, plataformas utilizadas e suas tagnames. Também haverá um botão para editar algumas informações.

Os Termos e Condições de Uso e as Políticas de Privacidade estarão à disposição para consulta do usuário a qualquer tempo.

O botão Configurações pode ser acessado para alterar as configurações de imagens e sons da plataforma. As FAQs podem ser consultadas nessa área.

O botão Suporte é o meio que o usuário possui para entrar em contato com a MatchBet e reportar quaisquer problemas, enviar informações, sugestões, reclamações e elogios e tirar suas dúvidas quanto à utilização do site.

Quando o usuário clicar em Encerrar Sessão sua sessão será finalizada e ele deverá realizar um novo login na MatchBet da próxima vez que quiser entrar em sua conta.

## MatchBet Bank
No MatchBet Bank o usuário tem acesso às funcionalidades e informações financeiras de sua conta. Esse é o campo que traz todas as informações acerca das Betcoins que o usuário possui, suas transações, perdas e ganhos dentro da MatchBet. É um espaço que conecta o usuário ao sistema financeiro da plataforma.

Aqui o usuário poderá se conectar a sua carteira do MetaMask e transferir Betcoins para a sua conta de usuário. Ele também poderá realizar saques da sua conta de usuário para a sua carteira MetaMask.

Quando a plataforma MatchBet for lançada, os usuários deverão adquirir suas Betcoins nas exchanges, como a Pancakeswap, por exemplo. A Betcoin é um criptoativo e, portanto, deverá ser adquirida do mesmo modo como são adquiridas as moedas: no mercado. Assim, com as Betcoins em sua carteira o usuário deverá logar na plataforma e descarregar a quantidade que desejar das suas Betcoins na sua conta de usuário.

Há também a opção de transferência de Betcoins para outros usuários e, nesse sentido, conterá informações de Betcoins recebidas de outros usuários. Ele também poderá consultar o seu saldo de Betcoins ou visualizar o seu extrato detalhado contendo todas as informações financeiras de sua conta em ordem cronológica.

No MatchBet Bank estarão listadas todas as taxas que a MatchBet retém em suas transações e um botão de Suporte exclusivo para informações relativas somente à área financeira.

É nesse espaço que o usuário poderá visualizar quanto de bônus ganhou com as suas indicações e, caso o usuário possua NFTs que lhe concedem benefícios financeiros, é aqui que ele verá o quanto de Betcoins ele está acumulando com esses benefícios de maneira detalhada.

No MatchBet Bank o usuário poderá visualizar e consultar todo o seu histórico financeiro, com todas as entradas e saídas de Betcoins da sua conta relacionadas a todas as funcionalidades da MatchBet como: valores das disputas e torneios, indicações, benefícios, transferências, ranking, bônus, punições e pedidos na loja.

## Validação de Vitórias
A MatchBet utiliza um validador de vitórias automático para reconhecer o ganhador de uma partida. Ele funciona em tempo real, seu código único já foi devidamente registrado nos órgãos governamentais competentes, e deve ser instalado no celular do usuário através das lojas iOS ou Android.

O usuário então ao abrir pela primeira vez o validador em seu celular, deverá fazer o login e conectar a sua conta MatchBet ao validador, que reconhecerá e gravará o seu número de celular vinculado a sua conta MatchBet.

Ao apontar o validador para a tela de encerramento do jogo, o validador reconhecerá automaticamente o vencedor da disputa, com base em um sistema registrado e inédito de reconhecimento de tela. Os jogos que possuem a validação automática são aqueles que a MatchBet indica na sua lista de jogos.

O usuário também terá a opção de streaming de suas partidas, para dar ainda maior assertividade quanto ao ganhador da disputa.

As validações de vitória somente serão utilizadas caso haja uma controvérsia quanto ao vencedor da disputa no preenchimento dos resultados. No entanto, essa controvérsia acarretará consequências quando se puder provar com certeza quem foi o usuário vencedor da disputa. Os usuários não saberão se o seu oponente está gravando a partida através de um streaming via MatchBet. Por isso, a honestidade é sempre o melhor caminho. A desonestidade trará um prejuízo muito grande para o oponente que confirmar um resultado diferente daquele que aconteceu.

A MatchBet irá alertar os usuários de todas as consequências que poderão advir de um preenchimento errado do resultado da partida. E, quando a MatchBet, por algum motivo, não conseguir apurar o real vencedor da partida, os Betcoins serão devolvidos aos usuários integralmente. Entretanto, a MatchBet monitorará esses usuários envolvidos para verificar se se trata de uma prática recorrente de utilização da plataforma (Blacklist).

As punições para os usuários que não estiverem usando a MatchBet de uma maneira honesta irão de advertências até o banimento do usuário do site, com o congelamento de todo o seu saldo em conta e bloqueio de todos os dados de acesso, incluindo o número de celular. As punições serão discricionárias da MatchBet e decididas caso a caso, a depender da frequência e do grau de desonestidade. Essas decisões são finais e irrecorríveis, mas durante o processo todos os usuários terão a oportunidade de se explicar.

Assim, o preenchimento errado de um resultado não trará prejuízos financeiros à MatchBet ou a quaisquer dos usuários, ou mesmo qualquer bônus em tentar ganhar indevidamente suas Betcoins nas disputas, porque o seu oponente irá reclamar e partida será revista e as Betcoins, no máximo, serão devolvidas, sem ganhos para quaisquer das partes. Todavia, a MatchBet passará a monitorar seu comportamento dentro da plataforma e, se constatarmos que é um comportamento reiterado, a MatchBet irá entrar em contato.

Eventualmente, ao longo do tempo, a MatchBet filtrará os usuários ruins dos usuários que utilizam a plataforma de uma forma honesta, contribuindo para um ambiente saudável e uma grande oportunidade de entretenimento e crescimento para a indústria dos jogos eletrônicos.

## Indicação de jogos pelos Usuários
Os usuários poderão incluir jogos na MatchBet em seus perfis pessoais. As partidas, nesses jogos, poderão ser realizadas somente com usuários da sua Lista de Amigos, inclusive com a criação de torneios. O usuário preencherá todas as informações desses jogos na aba Jogos Pessoais e, após adicioná-los, eles estarão listados na aba Jogos Pessoais. As disputas e torneios nesses jogos terão seu início de maneira semelhando aqueles jogos indicados pela MatchBet.

Todavia, nenhuma partida nesses jogos incluídos pelos usuários terá validação da MatchBet e o resultado será preenchido pela conta e risco dos usuários que aceitarem o convite para a disputa.

Nesses casos, caso haja uma divergência no resultado de vitória, a MatchBet irá reter o seu rake e devolver o restante das Betcoins para os oponentes igualmente. É por essa razão que esses jogos só poderão ser disponibilizados para usuários integrantes da Lista de Amigos.

## Chat
Todas as disputas e torneios possuem um chat entre os usuários, para que seja facilitada a comunicação entre eles e as partidas possam ocorrer da melhor maneira. O chat nas disputas pode ser acessado somente pelos dois adversários e o chat no torneio fica na sala do torneio e todos os seus integrantes podem se manifestar.

No chat são proibidas quaisquer formas de preconceito ou assédio, bem como ofensas pessoais de quaisquer tipos ou mensagens de cunho pornográfico ou violento, vocabulário vulgar e vilipêndio à fé. O chat foi desenvolvido para desenvolver a interação entre os usuários e ele não deve ser utilizado como um veículo de desinformação e protesto.

## Sistema de Parcerias
A MatchBet selecionará alguns parceiros para trabalhar em conjunto com a plataforma. Esses parceiros terão uma bonificação, negociada caso a caso, no percentual a ser percebido com as indicações e torneios.
Além disso, os parceiros poderão criar torneios exclusivos. Isso decorre do potencial de influência dessas pessoas na divulgação da MatchBet para novos membros.

Outrossim, é importante ressaltar que nada impede um usuário, que se destaque na MatchBet, em decorrência do seu grande potencial de trazer pessoas para a plataforma, de ser convidado a fazer parte do nosso rol de parceiros. É uma contrapartida pelo seu esforço em crescer conosco.

## Votações em decisões administrativas da MatchBet
Os usuários cadastrados, que sejam detentores de Betcoins em sua conta de usuário, poderão participar de enquetes e votações relativas a algumas decisões administrativas da MatchBet.

Essas votações ou enquetes off-chain serão disponibilizadas na página inicial do usuário e o resultado final será divulgado nos informativos da MatchBet.

Portanto, os usuários poderão participar, em conjunto com a MatchBet, da evolução da nossa plataforma e nos ajudar a melhorar a sua experiência conosco.

## SAC
Os usuários têm o seu canal de atendimento com a MatchBet. Através do nosso Serviço de Atendimento ao Consumidor – SAC, eles poderão entrar em contato com a MatchBet, a qualquer momento, para tirar suas dúvidas, resolver qualquer problema relacionado à plataforma, enviar sugestões, reclamações ou prestar quaisquer informações, através do e-mail: [info@matchbet.io](mailto:info@matchbet.io) ou no campo SAC que estará disponível no menu de sua página inicial.
