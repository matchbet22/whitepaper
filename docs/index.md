# MatchBet
A MatchBet foi desenvolvida, primeiramente, para democratizar o mercado de games online. O conceito da plataforma é intermediar disputas de games online competitivos de habilidade, entre usuários ou time de usuários, onde eles só podem apostar em si mesmos, em jogos já amplamente difundidos no mercado mundial de games, externos à blockchain, tais como FPS, Battle Royale e MOBA, como: LOL, CSGO, DOTA2, PUBG, etc., utilizando para a isso o nosso criptoativo, a Betcoin ($BET). E, em um curto espaço de tempo, jogos incluídos pela MatchBet e outros que os próprios usuários poderão incluir em seus perfis.

Essas disputas entre os usuários podem ser feitas em jogos de consoles ou em jogos para PC e, futuramente, na plataforma mobile.

Desse modo, para que isso funcione da melhor maneira, a MatchBet desenvolveu um sistema de reconhecimento de vitórias único no mercado, registrado, que atua em tempo real, com duas etapas de verificação. E, além disso, diversos mecanismos que praticamente impossibilitam qualquer tipo de fraude. Portanto, dinâmico e muito seguro.

A MatchBet não é um projeto que nasceu nos últimos dias ou nos últimos meses. Ela é um projeto que vem sendo desenhado há mais de 3 anos. Nesse período, de diversas reuniões, esquemas, esboços, ideias e decisões, já tivemos um grande investimento privado, que possibilitou a programação de praticamente toda a parte estrutural da plataforma. É por isso que a entrega das diversas ferramentas pode ser realizada em um curto prazo, conforme se pode verificar a [documentação técnica](https://app.swaggerhub.com/apis/StormGroup1/MatchBet/1.0)

A plataforma MatchBet tem um diferencial, quando comparado com outros projetos de criptoativos, pois o seu principal objetivo é não apenas ser rentável, mas também, sustentável a longo prazo, uma vez que a sua moeda, a Betcoin, será lastreada com a entrada contínua de capital, tanto dos investidores, quanto dos jogadores.

Para isso, foi desenvolvida uma estratégia de incentivo à entrada de novos usuários, marketing de indicação, e criação de torneios pelos nossos membros, com uma regra de negócios vantajosa a todos (investidor, jogador e plataforma).

Nesse mesmo sentido, as NFTs (Tokens não fungíveis) concebidas no projeto possuem, em sua maioria, um importante valor para os usuários, pois elas podem aumentar a sua capacidade de geração de renda na plataforma e, inclusive, a participação nos percentuais de lucro da MatchBet.

Os usuários têm um ambiente de controle de disputas amigável, possuindo, também, suas listas de amigos e todas as disputas tem interação por meio de chats. Além disso, podem criar e participar de torneios, ver sua posição no ranking, consultar seu inventário de NFTs, entrar na loja, dentre outras diversas funcionalidades.

Os usuários possuem amplo acesso ao seu histórico de disputas e torneios e ao seu extrato financeiro e, aqueles que possuírem as Betcoins, terão o privilégio de participar nas diversas decisões do direcionamento da plataforma MatchBet, em votações off-chain na página da home, um conceito extraído das fans tokens, colaborando para a evolução conectada com os anseios dos nossos usuários.
