# Tokenomics da Betcoin
A Betcoin ($BET) é o criptoativo que será usado para todas as transações no site MatchBet. É o componente essencial de toda a estrutura financeira da plataforma e, externamente, é o ativo que sofre variações no mercado e pode ser transacionado. A Betcoin será listada nas maiores corretoras de criptoativos do mundo.

## Pré-Venda
A MatchBet disponibilizará 20% de suas Betcoins para serem adquiridas nas fases de Pré-Venda. Elas serão distribuídas da seguinte forma:

<img width="800" src="/images/prevenda_11-04-2022.jpg" alt="">

Obviamente, levando apenas o preço da moeda em consideração, a PrivateSale é onde se encontra a Betcoin com o preço mais atrativo. Entretanto, o percentual disponibilizado aos investidores é menor.
## Quadro de Pré-Venda e Alocação de Betcoins detalhado:
<img width="800" src="/images/matchbet-tokenomics-12-04-2022.png" alt="">

## Public Sale/TGE
<img width="800" src="/images/publicsale_23-03-2022.jpg" alt="">

Observação importante: Caso não sejam vendidas todas as Betcoins nas etapas de Pré-venda e ICO, as Betcoins que restarem sofrerão um Burn. Dessa forma, vamos beneficiar aqueles que acreditaram no projeto e não vamos dar chances aqueles que ficam aguardando nas Exchanges. Em outras palavras, as etapas de Pré-venda serão o melhor momento de compra a um valor reduzido, pois o restante das Betcoins entra a mercado no time-lock em um período de 4 (quatro) anos. 

Após o período de Pré-Venda e ICO, a Betcoin será listada ao público em geral nas corretoras de criptoativos.

<img width="800" height="284" src="/images/tokenomicssummary.jpg" alt="">

## Release Schedule for $BET Token starting 22nd April 2022

<img width="800" src="/images/releaseschedule_17-03-2022.png" alt="">

## Airdrop

Você poderá participar do nosso lançamento indicando amigos, realizando tarefas e ganhando por cada tarefa realizada.
Informações: 
- Pool PRE-ICO: 20,000,000 $BETCOIN; 
- Pool Pós-ICO: 80,000,000 $BETCOIN;
- Atividade de cadastro: 5 $BETCOIN por usuário; 
- Atividade de indicação: 30 $BETCOIN por usuário; 
- Top 1,000 indicadores: 5,600,000 $BETCOIN;
- Fim da campanha PRE-ICO: 5 de julho de 2022;
- Distribuição da campanha PRE-ICO: 10 de julho de 2022;




### Regras
- Não é necessário o KYC para receber os tokens bonificados.
- As pessoas a quem você indica, devem participar de todas as tarefas de airdrop para serem contadas como uma indicação válida.
- A quantidade de pessoas indicadas por alguem é ilimitada.
- Contas múltiplas ou falsas não são permitidas e serão eliminadas.
- Spamming no grupo do Telegram não é permitido e não será tolerado.
- Se seus dados enviados estiverem errados, você poderá reenviar os dados novamente antes que o airdrop termine;
- Você deve continuar a seguir as tarefas das quais participou até que a distribuição do airdrop seja concluída;

Regras de bonificação para os maiores promotores da MatchBet, de acordo com sua posição no ranking: 
- Da 1º a 10º posição = 200,000 $ BETCOIN cada 
- Da 11º a 100º posição = 20,000 $ BETCOIN cada
- Da 101º a 1000º posição = 2,000 $ BETCOIN cada

Para quaisquer dúvidas entre em contato através de nosso canal no telegram: 
https://t.me/matchbetprevenda

## Fees and Temp-Bans
A empresa se reserva no direito de poder aplicar fees nas transações e o contrato impede que a empresa configure fees que o total ultrapasse 10%.

### Reflection fee
- Um valor de toda transação é removido e distribuido proporcionalmente a todos os holders. [Reference](https://www.axi.com/int/blog/education/blockchain/reflection-tokens)

### Staking fee
- Uma proporção de fees é removida para pagar incentivos de staking de maneira customizada. A empresa se reserva no direito de descrever como serão estes incentivos de staking em um contrato futuro.

### Ecosystem fee (community fund)
- Fee que a empresa recebe para sustentar e manter suas operações a longo prazo. Podendo ser usado com discricionariedade pela empresa.

### Burn fee
- Fee que causa escassez de moeda. Uma porção dos tokens são queimados causando deflação e beneficiando holders que nao transacionam frequentemente.

### Liquidity fee
- Parte das fees serão usadas para adicionar liquidez na pool.
1. Metade dos tokens de liquidity fee são vendidos a mercado na DEX e convertidos no outro elemento do DexPair. Neste momento, 50% do fee original se encontra convertido no outro elemento do par e 50% permanece no token original;
2. Adiciona-se liquidez na DEX usando os valores acima;
3. A empresa pode escolher ser a detentora dos comprovantes do depósito na DEX ou devolver eles aos clientes, na forma de cashback, incentivando que eles façam staking na DEX e recebam dividendos diretamente da DEX.

### AntiBot TimeLock DEX
- Para evitar a existência de bots, e desestimular sua atuação, cada wallet depois de uma operação em dex será banida temporariamente. O limite máximo que o contrato permite que a empresa aplique é de 1 dia.

### Circuit-Breaker fee
- O circuit breaker visa evitar despejo valores na DEX, evitando assim que o tokem desvalorize rapidamente. O mecanismo consiste em incidir taxas extras de fee de reflection que irão progressivamente atuar partir de um target inicial de queda. Daí quanto mais queda houver, maiores serão as fees. As taxas extras partem de 0% até tornar a venda não viável.
  * Ex. Em uma janela móvel de um dia, o mínimo de valor de queda que começa a ativar fees de Circuit-Breaker é de 10%(incidência de fee extra de 0%) e o máximo é de 20%(incidindo fee extra de 100%, efetivamente travando a transação de venda). Após atingir o mínimo, em toda transação de venda incidirá taxa extra de reflection crescendo linearmente de 0% a quando tiver caído 10% até chegar a 100% de taxas ao bater 20% de queda.
