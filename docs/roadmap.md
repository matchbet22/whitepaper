# Roadmap
## Primeiro Trimestre (Concluído)
- Investimento Privado
- Aquisições de domínios web e nas redes sociais
- Definições de nome oficial e identidade visual da marca
- Definição de regras e estrutura geral
- Definição da usabilidade de NFTs e Projetos Sociais vinculados
- Elaboração dos aspectos da Betcoin
- Programação das principais funcionalidades no backend – ver [Documentação Técnica](https://app.swaggerhub.com/apis/StormGroup1/MatchBet/1.0).

## Segundo Trimestre
- Lançamento oficial do website MatchBet.io
- Elaboração do Whitepaper e do Roadmap
- Textos de Press Release
- Confecção dos contratos (smart contract)
- Pré-Venda da Betcoin
- Abertura do ICO
- Listagem da Betcoin DAPP (ex. Pancake Swap)
- Designação do fluxo
- Aprovação das telas de UX
- Desenvolvimento de todas as telas
- Desenvolvimento da Estrutura Financeira
- Desenvolvimento do sistema de indicação e bonificação
- Desenvolvimento do envio automático de e-mails

## Terceiro Trimestre
- Aplicação de tecnologia Blockchain
- Design das NFTs
- Desenvolvimento da loja de NFTs
- Entrega da loja de NFTs
- Desenvolvimento do Admin
- Integração e entrega do Duelo com amigos sem validação
- Integração e entrega do Torneio sem validação

## Quarto Trimestre
- Integração das rotinas de validação nos Duelos com amigos e Torneios
- Integração e habilitação do Ranking
- Entrega dos Duelos com Amigos e Torneios com validação
- Entrega dos Duelos Randômicos
- Entrega da inclusão de Jogos pelos Usuários
- Desenvolvimento do sistema de Parcerias
- Desenvolvimento dos Torneios Abertos
